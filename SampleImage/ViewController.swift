//
//  ViewController.swift
//  SampleImage
//
//  Created by Wing Specialized Bank on 5/13/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    
    private let images: [UIImage] = [#imageLiteral(resourceName: "image4"), #imageLiteral(resourceName: "image2"), #imageLiteral(resourceName: "image3"), #imageLiteral(resourceName: "image1"), #imageLiteral(resourceName: "image5")]
    private var currentIndex: Int = 0 {
        didSet {
            imageView.image = images[currentIndex]
        }
    }
    private var count: Int {
        return images.count
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentIndex = 0
    }
    
    @IBAction func onPreviousButtonTap(_ sender: Any) {
        if currentIndex == 0 {
            currentIndex = count - 1
        }else {
            currentIndex -= 1
        }
    }
    
    @IBAction func onNextButtonTap(_ sender: Any) {
        if currentIndex == count - 1 {
            currentIndex = 0
        }else {
            currentIndex += 1
        }
    }
}

